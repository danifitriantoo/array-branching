#include <iostream>

using namespace std;

main () {
	
	int arr[5], eArr,max=0,min=0;
	
	cout << " Banyaknya Element Array : ";
	cin >> eArr;
	cout << "\n";
	
	// + 1 karena increment terakhir digunakan untuk menjalankan true condition
	for(int i = 0; i < (eArr + 1); i++)
	{
		if(i == eArr) {	
		
			// define max and min
			max = -1000;
			min = 1000;
			
			// true condition
			for(int j = 0; j < eArr; j++)
			{
				if(arr[j] > max)
				{
					max = arr[j];
				} 
					
				if(arr[j] < min) {
					min = arr[j];
				}
			}
		} else {
			
			// false condition
			cout << " Array Indeks ke-" << i << " : ";
			cin >> arr[i];
			
		}
	}
	
	cout << "\n Maksimum : " << max << endl;
	cout << " Minimum : " << min;
	
}
